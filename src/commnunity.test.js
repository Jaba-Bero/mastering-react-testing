import   Community  from './community';
import { render, cleanup, screen, waitForElement  } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { react } from 'react';
import store from './store/join';
import { Provider } from 'react-redux';
import '@testing-library/jest-dom'

describe('Community component', () => {
    test('renders "Big Community of People Like You" when section is shown', () => {
        render(<Provider store={ store }> <Community /> </Provider>)
        const h2 = screen.getByText('Big Community of People Like You');
        expect(h2).toBeInTheDocument();
    })

    test('renders "Hide Section" button ', () => {
        render(<Provider store={ store }> <Community /> </Provider>)
        const btn = screen.getByText('Hide Section');
        expect(btn).toBeInTheDocument();
    })

    test('renders "Show Section" when button is toggled', () => {
        render(<Provider store={ store }> <Community /> </Provider>)
        const buttonElement = screen.getByRole('button')
        userEvent.click(buttonElement)
        const btn = screen.getByText('Show Section');
        expect(btn).toBeInTheDocument();
    })

    test(' does not renders users story "Lorem ipsum dolor" when section is hidden ', () => {
        render(<Provider store={ store }> <Community /> </Provider>)
        const buttonElement = screen.getByRole('button')
        userEvent.click(buttonElement)
        const p = screen.queryByText('Lorem ipsum dolor');
        expect(p).toBeNull();
    })
})