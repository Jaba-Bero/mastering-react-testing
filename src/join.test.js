import   Join  from './join';
import { render, cleanup, screen, waitForElement  } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { react } from 'react';
import store from './store/join';
import { Provider } from 'react-redux';
import '@testing-library/jest-dom'

describe('Join component', () => {
    test('renders "Join Our Program" when page is loaded', () => {
        render(<Provider store={ store }> <Join /> </Provider>)
        const h2 = screen.getByText('Join Our Program');
        expect(h2).toBeInTheDocument();
    })

    test('renders "subscribe" button when page is loaded', () => {
        render(<Provider store={ store }> <Join /> </Provider>)
        const h2 = screen.getByText('SUBSCRIBE');
        expect(h2).toBeInTheDocument();
    })

    test('renders "subscribe" button ', () => {
        render(<Provider store={ store }> <Join /> </Provider>)
        const btn = screen.getByRole('button');
        expect(btn).toBeInTheDocument();
    })

})