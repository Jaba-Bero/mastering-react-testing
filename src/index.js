import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Community from './community';
import Join from './join';
import { Provider } from 'react-redux';
import store from './store/join';
import  App  from './app'
import {BrowserRouter as Router} from "react-router-dom"

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={ store }>
        <Router>
          <App />
        </Router>
    </Provider>
)


