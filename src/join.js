/* eslint-disable no-mixed-operators */
import { useSelector, useDispatch } from 'react-redux';

function Join() {
    const dispatch = useDispatch();
    const button = useSelector(state => state.button);

    const subscribeHandler = () => {
        dispatch({ type: 'subscribe' })
    }

    return (
        <section className='added-section'>
            <div className='added-section__content'>
                <h2 className='added-section__h2'>Join Our Program</h2>
                <p className='added-section__p' >Sed do eiusmod tempor incididunt<br/>ut labore et dolore magna aliqua.</p>
                <form className='added-section__email-box' id="added-section__email-box">
                    <input className='added-section__email-input' id='added-section__email-input' type="email" placeholder='Email'></input>
                    <button className='added-section__email-btn' id='added-section__email-btn' onClick={subscribeHandler} type='button'>{button}</button>
                </form>
            </div>
        </section>
    );
} 

export default Join;